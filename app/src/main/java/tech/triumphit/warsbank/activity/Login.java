package tech.triumphit.warsbank.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.triumphit.warsbank.R;
import tech.triumphit.warsbank.databinding.ActivityLoginBinding;
import tech.triumphit.warsbank.dto.LoginRequestDto;
import tech.triumphit.warsbank.dto.LoginResponseDto;
import tech.triumphit.warsbank.service.ApiService;
import tech.triumphit.warsbank.utils.Constants;

public class Login extends AppCompatActivity {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        binding.content.signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, SignUp.class));
            }
        });

        binding.content.signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkAllData()){
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ApiService apiService = retrofit.create(ApiService.class);

                    LoginRequestDto dto = new LoginRequestDto();
                    dto.setEmail(binding.content.email.getText().toString());
                    dto.setPassword(binding.content.password.getText().toString());

                    apiService.login("2", binding.content.email.getText().toString(), binding.content.password.getText().toString()).enqueue(new Callback<LoginResponseDto>() {
                        @Override
                        public void onResponse(Call<LoginResponseDto> call, Response<LoginResponseDto> response) {
                            if(response.code() == HttpURLConnection.HTTP_OK){
                                if(response.body().getError().equals("false")){
                                    if(response.body().getMessage().getPhoneVerify().equals("Unverified")){
                                        startActivity(new Intent(Login.this, ActivityVerifiPhoneNumber.class));
                                    }else{
                                        startActivity(new Intent(Login.this, Home.class));
                                    }
                                    Log.e("phone", response.body().getMessage().getPhoneVerify());
                                }else {
                                    Toast.makeText(Login.this, "Server Error", Toast.LENGTH_SHORT).show();
                                }

                            }else {
                                Toast.makeText(Login.this, "Server Error", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponseDto> call, Throwable t) {
                            Toast.makeText(Login.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("phone", t.getMessage());
                        }
                    });

                }else{
                    Snackbar.make(binding.getRoot(), "Please Fill Up All Data", Snackbar.LENGTH_LONG).show();
                }


            }
        });

    }

    private boolean checkAllData() {

        if(binding.content.email.getText().toString().equals("")){
            return false;
        }

        if(binding.content.password.getText().toString().equals("")){
            return false;
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
