package tech.triumphit.warsbank.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.triumphit.warsbank.R;
import tech.triumphit.warsbank.databinding.ActivitySignUpBinding;
import tech.triumphit.warsbank.dto.SignupRequestDto;
import tech.triumphit.warsbank.dto.SignupResponseDto;
import tech.triumphit.warsbank.service.ApiService;
import tech.triumphit.warsbank.utils.Constants;

public class SignUp extends AppCompatActivity {

    ActivitySignUpBinding binding;
    ProgressDialog pd;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pd = new ProgressDialog(this);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        binding.content.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkAllData()){
                    if(binding.content.password.getText().toString().length() > 7){
                        if(binding.content.password.getText().toString().equals(binding.content.passwordConfirm.getText().toString())){
                            if(binding.content.phone.getText().toString().length() > 9){
                                pd.setMessage("Please Wait");
                                pd.show();
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl(Constants.BASE_URL)
                                        .addConverterFactory(GsonConverterFactory.create())
                                        .build();
                                final ApiService apiService = retrofit.create(ApiService.class);

                                SignupRequestDto dto = new SignupRequestDto();
                                dto.setOut("1");
                                dto.setName(binding.content.name.getText().toString());
                                dto.setSurname(binding.content.surname.getText().toString());
                                dto.setEmail(binding.content.email.getText().toString());
                                dto.setPhonenumber(binding.content.phone.getText().toString());
                                dto.setBirthday(binding.content.birthday.getText().toString());
                                dto.setPassword(binding.content.password.getText().toString());
                                Log.e("data", dto.toString());
                                apiService.signup(1, dto.getName(), dto.getSurname(), dto.getEmail(), dto.getPassword(), dto.getPhonenumber(), dto.getBirthday()).enqueue(new Callback<SignupResponseDto>() {
                                    @Override
                                    public void onResponse(Call<SignupResponseDto> call, Response<SignupResponseDto> response) {
                                        if(response.code() == HttpURLConnection.HTTP_OK){
                                            if(response.body().getError().equals("false")){
                                                Toast.makeText(SignUp.this, "Kayıt başarıyla tamamlandı! ", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }else {
                                                pd.dismiss();
                                                Snackbar.make(binding.getRoot(), response.body().getError(), Snackbar.LENGTH_LONG).show();
                                            }
                                        }
                                        Log.e("response", response.body().getMessage());
                                        pd.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<SignupResponseDto> call, Throwable t) {
                                        Log.e("response", t.getMessage());
                                        pd.dismiss();
                                    }
                                });

//                                apiService.signup(dto).enqueue(new Callback<SignupResponseDto>() {
//                                    @Override
//                                    public void onResponse(Call<SignupResponseDto> call, Response<SignupResponseDto> response) {
//                                        if(response.code() == HttpURLConnection.HTTP_OK){
//                                            if(response.body().getMessage().equals("false")){
//                                                Toast.makeText(SignUp.this, "Kayıt başarıyla tamamlandı! ", Toast.LENGTH_SHORT).show();
//                                                Log.e("Error", response.body().getError());
//                                                Log.e("Message", response.body().getMessage());
//                                                pd.dismiss();
//                                                finish();
//                                            }else{
//                                                pd.dismiss();
//                                                Log.e("Error", response.body().getError());
//                                                Log.e("Message", response.body().getMessage());
//                                                Snackbar.make(binding.getRoot(), response.body().getError(), Snackbar.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<SignupResponseDto> call, Throwable t) {
//
//                                    }
//                                });
                            }else{
                                Snackbar.make(binding.getRoot(), "Geçersiz telefon numarası girdiniz!", Snackbar.LENGTH_LONG).show();
                            }

                            
                        }else {
                            Snackbar.make(binding.getRoot(), "Şifre eşleşmiyor", Snackbar.LENGTH_LONG).show();
                        }
                    }else{
                        Snackbar.make(binding.getRoot(), "Şifreniz 8 karekterden küçük olamaz!", Snackbar.LENGTH_LONG).show();
                    }
                }else {
                    Snackbar.make(binding.getRoot(), "Lütfen Tüm Alanları Doldurun!", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        binding.content.birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(999);
            }
        });

    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private boolean checkAllData() {
        if(binding.content.name.getText().toString().equals("")){
            return false;
        }
        if(binding.content.surname.getText().toString().equals("")){
            return false;
        }
        if(binding.content.phone.getText().toString().equals("")){
            return false;
        }
        if(binding.content.birthday.getText().toString().equals("")){
            return false;
        }
        if(binding.content.email.getText().toString().equals("")){
            return false;
        }
        if(binding.content.password.getText().toString().equals("")){
            return false;
        }
        if(binding.content.passwordConfirm.getText().toString().equals("")){
            return false;
        }
        return true;
    }

    private void showDate(int year, int month, int day) {
        binding.content.birthday.setText(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
    }


}
