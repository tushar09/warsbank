package tech.triumphit.warsbank.activity;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.databinding.tool.DataBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tech.triumphit.warsbank.R;
import tech.triumphit.warsbank.databinding.ActivityVerifiPhoneNumberBinding;

public class ActivityVerifiPhoneNumber extends Activity {

    ActivityVerifiPhoneNumberBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verifi_phone_number);
    }
}
