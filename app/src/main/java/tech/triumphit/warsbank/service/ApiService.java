package tech.triumphit.warsbank.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import tech.triumphit.warsbank.dto.LoginRequestDto;
import tech.triumphit.warsbank.dto.LoginResponseDto;
import tech.triumphit.warsbank.dto.SignupRequestDto;
import tech.triumphit.warsbank.dto.SignupResponseDto;

/**
 * Created by Tushar on 1/23/2017.
 */

public interface ApiService {
    @POST("_ajax_v1.php")
    @FormUrlEncoded
    Call<LoginResponseDto> login(@Field("out") String out, @Field("email") String email, @Field("password") String password);


    @POST("_ajax_v1.php")
    @FormUrlEncoded
    Call<SignupResponseDto> signup(@Field("out") int out, @Field("name") String name, @Field("surname") String surname, @Field("email") String email, @Field("password") String password, @Field("phonenumber") String phonenumber, @Field("birthday") String birthday);
}
