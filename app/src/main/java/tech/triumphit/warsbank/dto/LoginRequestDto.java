package tech.triumphit.warsbank.dto;

/**
 * Created by Tushar on 1/23/2017.
 */

public class LoginRequestDto {
    public String out, password, email;

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "LoginRequestDto{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", out='" + out + '\'' +
                '}';
    }
}
