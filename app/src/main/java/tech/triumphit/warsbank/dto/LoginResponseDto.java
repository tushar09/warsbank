package tech.triumphit.warsbank.dto;

/**
 * Created by Tushar on 1/23/2017.
 */

public class LoginResponseDto {
    String error;
    message message;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LoginResponseDto.message getMessage() {
        return message;
    }

    public void setMessage(LoginResponseDto.message message) {
        this.message = message;
    }

    public class message{
        String PhoneVerify, PhoneNumber, Email, PassWord;

        public String getPhoneVerify() {
            return PhoneVerify;
        }

        public void setPhoneVerify(String phoneVerify) {
            PhoneVerify = phoneVerify;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPassWord() {
            return PassWord;
        }

        public void setPassWord(String passWord) {
            PassWord = passWord;
        }


        @Override
        public String toString() {
            return "message{" +
                    "PhoneVerify='" + PhoneVerify + '\'' +
                    ", PhoneNumber='" + PhoneNumber + '\'' +
                    ", Email='" + Email + '\'' +
                    ", PassWord='" + PassWord + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LoginResponseDto{" +
                "error='" + error + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
