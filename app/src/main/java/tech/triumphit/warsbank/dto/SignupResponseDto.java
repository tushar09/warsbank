package tech.triumphit.warsbank.dto;

/**
 * Created by Tushar on 1/24/2017.
 */

public class SignupResponseDto {
    String error;
    String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SignupResponseDto{" +
                "error=" + error +
                ", message='" + message + '\'' +
                '}';
    }
}
